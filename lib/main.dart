import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:my_health_app/model_cls/LoginModelCls.dart';
import 'package:my_health_app/healthList.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_health_app/UniversalCls.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  FocusNode _focusUserName = new FocusNode();
  FocusNode _focusPswd = new FocusNode();

  final userNameController = TextEditingController();
  final passwordController = TextEditingController();

  bool userBool = false, pswdBool = false;
  String email, pswd;

  @override
  void dispose() {
    // TODO: implement dispose
    userNameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    
    _focusUserName.addListener(_onFocusChangeUserName);
    /*_focusPswd.addListener(_onFocusChangePswd);*/
  }

  void _onFocusChangeUserName(){
    email = userNameController.text.toString().trim();
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    bool b = email.contains(new RegExp(emailPattern), 0);

    setState(() {
      if(email.length>0) {
        if (b) {
          userBool = false;
        } else {userBool = true;}
      }
    });
  }

  void postJsonData22() {
    email = userNameController.text.toString().trim();
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    pswd = passwordController.text.toString().trim();

    bool b = email.contains(new RegExp(emailPattern), 0);

    setState(() {
      if (b && email.length > 0) {
        if (pswd.length >= 8) {
          userBool = false;
          pswdBool = false;
          this.postJsonData();
        } else {pswdBool = true;}
      } else {userBool = true;}
    });
  }

  Future<String> postJsonData() async {

		UniversalCls cls = new UniversalCls();
		Map<String, String> headers = {
			 "App-Id": "CMAPP",
			 "Client-Type": "APP",
			 "Content-Type": "application/json"
		};
    String json = '{"email":"$email", "password":"$pswd"}';

    var response = await http.post(Uri.encodeFull(cls.url), headers: headers, body: json);

    // check the status code for the result
    int statusCode = response.statusCode;
    print("");print("");print("Status Code: $statusCode");print("");

    String body = response.body;
    var parseJson = jsonDecode(response.body);

    if (statusCode == 200) {

      //   var parseJson = json.decode(response.body);
      var loginModelCls = LoginModelCls.fromJsonMap(parseJson);

      /*
      print("kyc_verfication_required:  ${loginModelCls.data.kyc_verfication_required}");
      print("");print("");print("");print("");*/

      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => healthList()),
      );
    }else{
      Fluttertoast.showToast(
        msg: "\n  ${parseJson['message']}  \n  ",
        textColor: Colors.white,
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIos: 1,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black54,
      );
    }

    return "Success";
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
			 backgroundColor: Colors.white,
      body: new Center(
        child: SingleChildScrollView(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _logo(),
              Container(height: 20.0,),
              _sponsoredBy(),
							Container(height: 20.0,),
              _loginForm(),
            ],
          ),
        ),
      ),
    );
  }



	_loginForm(){
  	 return new Form(
				 child: new Theme(
						data: new ThemeData(
								brightness: Brightness.light,
								primarySwatch: Colors.blue,
								inputDecorationTheme: new InputDecorationTheme(
										labelStyle: new TextStyle(
												color: Colors.black54, fontSize: 20.0
										)
								)
						),
						child: new Container(
							 padding: const EdgeInsets.all(40.0),
							 child: new Column(
									crossAxisAlignment: CrossAxisAlignment.center,
									children: <Widget>[
										 new TextFormField(
												decoration: new InputDecoration(
													 labelText: "Email",
													 errorText: userBool ? "Invalid email address" : null,
													 hintText: "Enter Email",
													 labelStyle: TextStyle(
															 color: Colors.black38,
															 fontWeight: FontWeight.bold
													 ),
													 hintStyle: TextStyle(
															 color: Colors.black38,
															 fontWeight: FontWeight.bold
													 ),
												),
												keyboardType: TextInputType.emailAddress,
												style: TextStyle(color: Colors.black),
												controller: userNameController,
												focusNode: _focusUserName,
										 ),
										 new TextFormField(
												decoration: new InputDecoration(
														labelText: "Password",
														errorText: pswdBool ? "Please enter correct password" : null,
														hintText: "Enter Password",
														labelStyle: TextStyle(
																color: Colors.black38,
																fontWeight: FontWeight.bold),
														hintStyle: TextStyle(
																color: Colors.black38,
																fontWeight: FontWeight.bold
														)
												),
												keyboardType: TextInputType.text,
												style: TextStyle(color: Colors.black),
												obscureText: true,
												controller: passwordController,
												focusNode: _focusPswd,
										 ),
										 new Padding(
												padding: const EdgeInsets.only(top: 40.0),
										 ),
										 new MaterialButton(
												height: 50.0,
												minWidth: 160.0,
												color: Colors.blue,
												textColor: Colors.white,
												child: new Text(
													 "Sign In",
													 style: TextStyle(
															 fontSize: 22.0,
															 fontWeight: FontWeight.bold
													 ),
												),
												onPressed: postJsonData22,
												splashColor: Colors.redAccent,
										 )
									],
							 ),
						),
				 )
		 );
	}


  _logo(){
  	 return Container(
				padding: EdgeInsets.all(40.0),
				width: 210.0,
				height: 210.0,
				decoration: new BoxDecoration(
						color: const Color(0xff273040), shape: BoxShape.circle),
				child: Center(
					 child: new Container(
							child: Center(
								 child: new Text(
										"Mt Druitt Medical Centre Logo",
										style: TextStyle(
												color: Colors.white,
												fontSize: 22.0,
												fontWeight: FontWeight.bold),
										textAlign: TextAlign.center,
								 ),
							),
					 ),
				),
		 );
	}
	_sponsoredBy(){
  	 return Container(
				margin: const EdgeInsets.only(top: 20.0),
				child: new Image(
					 image: new AssetImage("assets/ecare.png"),
					 fit: BoxFit.fill,
					 width: 250.0,
					 height: 69.0,
				),
		 );
	}
}
