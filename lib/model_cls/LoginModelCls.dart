import 'package:my_health_app/model_cls/data.dart';

class LoginModelCls {

  bool success;
  String message;
  String token;
  Data data;

	LoginModelCls.fromJsonMap(Map<String, dynamic> map): 
		success = map["success"],
		message = map["message"],
		token = map["token"],
		data = Data.fromJsonMap(map["data"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data2 = new Map<String, dynamic>();
		data2['success'] = success;
		data2['message'] = message;
		data2['token'] = token;
		data2['data'] = data == null ? null : data.toJson();
		return data2;
	}
}
