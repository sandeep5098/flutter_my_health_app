
class Organisation {

  String id;
  String app_intro_image;

	Organisation.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		app_intro_image = map["app_intro_image"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['app_intro_image'] = app_intro_image;
		return data;
	}
}
