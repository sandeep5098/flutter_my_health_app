import 'package:my_health_app/model_cls/organisation.dart';

class Data {

  String id;
  Object patientInternalId;
  String orgId;
  Organisation organisation;
  String context;
  String email;
  String firstName;
  String surName;
  Object middleName;
  Object green_vid;
  int kyc_verfication_required;

	Data.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		patientInternalId = map["patientInternalId"],
		orgId = map["orgId"],
		organisation = Organisation.fromJsonMap(map["organisation"]),
		context = map["context"],
		email = map["email"],
		firstName = map["firstName"],
		surName = map["surName"],
		middleName = map["middleName"],
		green_vid = map["green_vid"],
		kyc_verfication_required = map["kyc_verfication_required"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['patientInternalId'] = patientInternalId;
		data['orgId'] = orgId;
		data['organisation'] = organisation == null ? null : organisation.toJson();
		data['context'] = context;
		data['email'] = email;
		data['firstName'] = firstName;
		data['surName'] = surName;
		data['middleName'] = middleName;
		data['green_vid'] = green_vid;
		data['kyc_verfication_required'] = kyc_verfication_required;
		return data;
	}
}
